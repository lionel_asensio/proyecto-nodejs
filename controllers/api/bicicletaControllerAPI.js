var Bicicleta = require('../../models/bicicleta');

//listar Bicicletas
exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

//Crear bicicletas
exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lon];

    Bicicleta.add(bici);
    res.status(200).json({biciletas: bici});
}

//Delete
exports.bicicletas_delete = function(req, res){
    let id = req.params.id;
    let bici = Bicicleta.findById(id);
    if(bici != null){
        Bicicleta.removeById(req.params.id);
        res.status(204).send();
    }
    else{
        res.status(500).send();
    }
}

//Update
exports.bicicletas_update = function(req, res){
    let id = req.params.id;
    let bici = Bicicleta.findById(id);
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lon];

    res.status(200).json({
        bicicleta: bici
    });
}