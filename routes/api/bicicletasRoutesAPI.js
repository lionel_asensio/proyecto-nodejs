var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create);
router.delete('/delete/:id', bicicletaController.bicicletas_delete);
router.put('/update/:id', bicicletaController.bicicletas_update);

module.exports = router;