var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return "id: " + this.id + " | color: " + this.color;
}

//Metodos
Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}
Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici) {
        return aBici;
    } else {
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
    }
}
Bicicleta.removeById = function(aBiciId){
    Bicicleta.findById(aBiciId);
    for (let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i,1);
            break;
        }        
    }
}


//Coleccion de bicibletas
var a = new Bicicleta(1,"rojo", "urbana", [-34.5990477,-58.4841845]);
var b = new Bicicleta(2,"blanca", "montaña", [-34.5980477,-58.4961845]);

//agregamos las bicicletas
Bicicleta.add(a);
Bicicleta.add(b);

//Exportar el modelo bicicleta
module.exports = Bicicleta;